/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carrara1u
 */
public class ExampleGenerator {

    public static class Example<X, Y> {

        private Y y;
        private X x;

        public Example(X x, Y y) {
            this.x = x;
            this.y = y;
        }

        public String toString() {
            NumberFormat formatter = new DecimalFormat("#0.00");     
            return "(" + formatter.format(x) + ";" + formatter.format(y) + ")";
        }

        /**
         * @return the y
         */
        public Y getY() {
            return y;
        }

        /**
         * @return the x
         */
        public X getX() {
            return x;
        }
    }

    public static List<Example<Double, Double>> generateRandomExamples(
            int number, String function, double xmin, double xmax) {
        List<Example<Double, Double>> examples = new ArrayList<>(number);
        try {
            Class class1 = Class.forName("model.Function");
//            System.out.println("class : " + Arrays.deepToString(class1.getDeclaredMethods()));
            Method method;
            method = class1.getMethod(function, double.class);
            for (int i = 0; i < number; i++) {
                int index = (int) (Math.random() * (examples.size() - 1));

                double x = xmin + (Math.random() * (xmax - xmin));
                double y;
                y = (double) method.invoke(null, x);
//                System.out.println("index  : " + index);
                examples.add(index, new Example(x, y));
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExampleGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(ExampleGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(ExampleGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ExampleGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ExampleGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(ExampleGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

        return examples;
    }

    public static List<Example<Double, Double>> normaliseExamplesAbcisse(
            List<Example<Double, Double>> examples) {

        double xmin = Double.MAX_VALUE;
        double xmax = Double.MIN_VALUE;

        for (Example<Double, Double> e : examples) {
            xmin = e.getX() < xmin ? e.getX() : xmin;
            xmax = e.getX() > xmax ? e.getX() : xmax;
        }

        for (Example<Double, Double> e : examples) {
            e.x = (e.getX() - xmin) / (xmax - xmin);
        }
//        System.out.println(""+examples);
        return examples;
    }
    public static List<Example<Double, Double>> normaliseExamplesOrdonnee(
            List<Example<Double, Double>> examples) {

        double xmin = Double.MAX_VALUE;
        double xmax = Double.MIN_VALUE;

        for (Example<Double, Double> e : examples) {
            xmin = e.getY() < xmin ? e.getY() : xmin;
            xmax = e.getY() > xmax ? e.getY() : xmax;
        }

        for (Example<Double, Double> e : examples) {
            e.y = (e.getY() - xmin) / (xmax - xmin);
        }
//        System.out.println(""+examples);
        return examples;
    }
}
