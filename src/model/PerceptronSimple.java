/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import model.ExampleGenerator.Example;

/**
 *
 * @author carrara1u
 */
public class PerceptronSimple extends Perceptron {

    // wx + teta
    double teta;
    double w;
    double teta_b;
    double w_b;
    //double alpha;

    public PerceptronSimple() {
        teta_b = Math.random()*2-1;
        w_b = Math.random()*2-1;
    }

    @Override
    public double predict(double x) {
        return w_b * x + teta_b;
    }

    @Override
    public void a_doAnEpoch(double alpha, List<Example<Double, Double>> base_learning) {
        int i = 0;
        boolean stop = false;
        while (i < base_learning.size() && !stop) {
            Example<Double, Double> e = base_learning.get(i);
            w = w_b + (alpha * ((e.getY() - predict(e.getX())) * e.getX()));
            teta = teta_b + (alpha * (e.getY() - predict(e.getX())));
            teta_b = teta;
            w_b = w;
            i++;
        }
    }

}
