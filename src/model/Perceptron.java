/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import model.ExampleGenerator.Example;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public abstract class Perceptron {

    private double current_error = Double.MAX_VALUE;

    public abstract double predict(double x);

    private int nbEpoch = 0;

    public void reset(){
        nbEpoch = 0;
        current_error = Double.MAX_VALUE;
    }
    
    public void learn(
            double alpha,
            List<Example<Double, Double>> base_learning,
            int nombre_epoch_max,
            double epsilon_error_to_stop,
            double error_to_stop) {
        boolean stop = false;
        while (!stop) {
            doAnEpoch(alpha, base_learning);
            double past_error = current_error;
            current_error = computeTotalQuadError(base_learning);
            // workaround pr éviter les nombres très proche de zéro
            stop = nbEpoch >= nombre_epoch_max
                    || Math.abs(current_error - past_error) - epsilon_error_to_stop <= (0.0000000001)
                    || current_error - error_to_stop <= (0.0000000001);
//            System.out.println("Math.abs(current_error - past_error)"+Math.abs(current_error - past_error));
//            System.out.println("current_error <= error_to_stop "+(current_error <= error_to_stop));
//            System.out.println("nbEpoch > nombre_epoch_max : "+(nbEpoch > nombre_epoch_max));
//            System.out.println("stop ? "+stop);
//            System.out.println(""+nbEpoch);
        }
    }

    protected abstract void a_doAnEpoch(double alpha, List<Example<Double, Double>> base_learning);

    public void doAnEpoch(double alpha, List<Example<Double, Double>> base_learning) {
//        System.out.println("epoch : " + nbEpoch);
        a_doAnEpoch(alpha, base_learning);
        nbEpoch++;

    }

    public double computeTotalQuadError(List<Example<Double, Double>> examples) {
        double errorTotal = 0;
        double erreurExample = 0;
        for (Example<Double, Double> e : examples) {
            erreurExample = computeSingleQuadError(e);
            errorTotal += erreurExample;
        }
        return errorTotal;
    }

    public double computeSingleQuadError(Example<Double, Double> e) {
        return Math.pow((predict(e.getX()) - e.getY()), 2) / 2.;
    }

    /**
     * @return the nbEpoch
     */
    public int getNbEpoch() {
        return nbEpoch;
    }

}
