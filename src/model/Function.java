/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author carrara1u
 */
public class Function {

    private static final String HEAD = "";
    public static final String SIGMOID = HEAD + "sigmoid";
    public static final String CELSUIUS_2_FAHRENHEIT = HEAD + "celsius2fahrenheit";
    public static final String SINUS = HEAD + "sinus";
    public static final String X_SQUARE = HEAD + "xsquare";
    public static final String X_ABS = HEAD + "xabs";
    public static final String RAND = HEAD + "rand";

    public static double celsius2fahrenheit(double celsius) {
        return celsius * 9. / 5. + 32.;
    }

    public static double rand(double x){
        return x + 100 * Math.random();
    }
    
    public static double sinus(double x) {
        return Math.sin(x);
    }

    public static double xsquare(double x) {
        return Math.pow(x, 2);
    }

    public static double xabs(double x) {
        return Math.abs(x);
    }

    public static double sigmoid(double x) {
        return 1. / (1. + Math.exp(-x));
    }
}
