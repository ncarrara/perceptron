/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import model.ExampleGenerator.Example;
import model.mlp.MLP;

/**
 *
 * @author carrara1u
 */
public class Model extends Observable {

    private List<Example<Double, Double>> base_test;
    private List<Example<Double, Double>> base_learning;

    private boolean reseted = true;

    private Perceptron p;

    private String current_perceptron = "MLP";

    private double alpha = 0.01;

    private String function = Function.X_SQUARE;

    private double error_to_stop = 0.1;

    private double error_epsilon_to_stop = 0.00001;

    private int epoch_to_stop = 4000;

    public Model() {
        reset();
        reseted = false;
    }

    public void runAtomique() {
        reseted = false;
        p.doAnEpoch(alpha, base_learning);
        update();
    }

    public final void reset() {

        List<Example<Double, Double>> l = ExampleGenerator.generateRandomExamples(100, function, -Math.PI, Math.PI);
        base_test = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            base_test.add(l.remove(i));
        }
        base_test = ExampleGenerator.normaliseExamplesAbcisse(base_test);
        base_test = ExampleGenerator.normaliseExamplesOrdonnee(base_test);
        base_learning = ExampleGenerator.normaliseExamplesAbcisse(l);
        base_learning = ExampleGenerator.normaliseExamplesOrdonnee(base_learning);

        switch (current_perceptron) {
            case "simple":
                p = new PerceptronSimple();
                break;
            case "MLP":
                p = new MLP();
                break;
        }
        reseted = true;
        update();
    }

    public void setPerceptron(String i) {
        this.current_perceptron = i;
//        switch (i) {
//            case "simple":
//                p = new PerceptronSimple();
//                break;
//            case "MLP":
//                p = new MLP();
//                break;
//        }
        reset();
    }

    public void update() {
        setChanged();
        notifyObservers();
    }

    public void learnWithoutCondition(int nbEpoch) {
        reseted = false;
        while (p.getNbEpoch() <= nbEpoch) {
            //runAtomique();
            p.doAnEpoch(alpha, base_learning);
        }
        update();

    }

    public void learnWithCondition() {
        reset();
        reseted = false;
//        System.out.println(""+epoch_to_stop);
        p.learn(alpha, base_learning, epoch_to_stop, error_epsilon_to_stop, error_to_stop);
//        System.out.println(""+p.getNbEpoch());
        update();
    }

    /**
     * @return the base_test
     */
    public List<Example<Double, Double>> getBase_test() {
        return base_test;
    }

    /**
     * @return the base_learning
     */
    public List<Example<Double, Double>> getBase_learning() {
        return base_learning;
    }

    /**
     * @return the p
     */
    public Perceptron getP() {
        return p;
    }

    /**
     * @return the function
     */
    public String getFunction() {
        return function;
    }

    /**
     * @param function the function to set
     */
    public void setFunction(String function) {
        this.function = function;
        reset();
    }

    /**
     * @return the reseted
     */
    public boolean isReseted() {
        return reseted;
    }

    /**
     * @return the alpha
     */
    public double getAlpha() {
        return alpha;
    }

    /**
     * @param alpha the alpha to set
     */
    public void setAlpha(double alpha) {
        this.alpha = alpha;
        update();
    }

    /**
     * @return the error_to_stop
     */
    public double getError_to_stop() {
        return error_to_stop;
    }

    /**
     * @param error_to_stop the error_to_stop to set
     */
    public void setError_to_stop(double error_to_stop) {
        this.error_to_stop = error_to_stop;
        reset();
    }

    /**
     * @return the error_epsilon_to_stop
     */
    public double getError_epsilon_to_stop() {
        return error_epsilon_to_stop;
    }

    /**
     * @param error_epsilon_to_stop the error_epsilon_to_stop to set
     */
    public void setError_epsilon_to_stop(double error_epsilon_to_stop) {
        this.error_epsilon_to_stop = error_epsilon_to_stop;
        reset();
    }

    /**
     * @return the epoch_to_stop
     */
    public int getEpoch_to_stop() {
        return epoch_to_stop;
    }

    /**
     * @param epoch_to_stop the epoch_to_stop to set
     */
    public void setEpoch_to_stop(int epoch_to_stop) {
        this.epoch_to_stop = epoch_to_stop;
        reset();
    }

}
