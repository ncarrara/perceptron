/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.mlp;

import java.util.List;
import model.ExampleGenerator;
import model.ExampleGenerator.Example;
import model.Perceptron;
import static model.mlp.Neuron.ActivationFunction.ID;
import static model.mlp.Neuron.ActivationFunction.TANH;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class MLP extends Perceptron {

    private final ConstantNeuron input;
    private final Neuron output;
    private final Neuron hidden1;
    private final Neuron hidden2;
    private final Neuron hidden0;
    private final Neuron[] hiddenLayer;
    private final ConstantNeuron c;

    public MLP() {
        input = new ConstantNeuron(Double.NaN);
        hidden0 = new Neuron(TANH, input);
        hidden1 = new Neuron(TANH, input);
        hidden2 = new Neuron(TANH, input);
        c = new ConstantNeuron(1);
        hiddenLayer = new Neuron[4];
        hiddenLayer[0] = hidden0;
        hiddenLayer[1] = hidden1;
        hiddenLayer[2] = hidden2;
        hiddenLayer[3] = c;
        output = new Neuron(ID, hidden0, hidden1, hidden2, c);
    }

    @Override
    public double predict(double x) {
        input.setValue(x);
        double y = output.compute();
        return y;
    }

    @Override
    protected void a_doAnEpoch(double alpha, List<Example<Double, Double>> base_learning) {
        int i = 0;
        boolean stop = false;
        while (i < base_learning.size() && !stop) {

            Example<Double, Double> e = base_learning.get(i);

            double y = predict(e.getX()); // valeur calculé
            double t = e.getY(); // valeur désiré
            
//            System.out.println("-- new example --" + e);
            // ces deux modifcations doivent se faire en parralèle
            // => necessité de sauvergarder les anciennes valeurs de poids
            double[] deltasOuputLayer = computeOuputLayerDelta(alpha, y, t);
            for (int j = 0; j < deltasOuputLayer.length; j++) {
                output.prepareToEvolve(j, deltasOuputLayer[j]);
            }
            double[] deltasHiddenLayer = computeHiddenLayerDelta(alpha, y, t);
            for (int j = 0; j < 3; j++) {
                hiddenLayer[j].prepareToEvolve(0, deltasHiddenLayer[j]);
            }
            //traitement parralèle
            evolveNeurons();
            i++;
        }
    }

    private void evolveNeurons() {
        output.evolve();
    }

    private double[] computeOuputLayerDelta(double alpha, double y, double t) {
        double[] deltas = new double[4];
        for (int j = 0; j < deltas.length; j++) {
            deltas[j] = alpha * (t - y) * hiddenLayer[j].compute();
        }
        return deltas;
    }

    private double[] computeHiddenLayerDelta(double alpha, double y, double t) {
        double[] deltas = new double[3];
        for (int j = 0; j < 3; j++) {
            deltas[j]
                    = - alpha
                    * (1 - Math.pow(hiddenLayer[j].compute(), 2))
                    * input.compute()
                    * -(t - y) * output.getPoids_in()[j];
        }
        return deltas;
    }

}
