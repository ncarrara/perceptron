/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.mlp;

import static model.mlp.Neuron.ActivationFunction.ID;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class ConstantNeuron extends Neuron {

    private double value;

    public ConstantNeuron(double value) {
        super(ID, new Neuron[0]);
        this.value = value;
    }

    @Override
    public double compute() {
        return value;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(double value) {
        this.value = value;
    }

}
