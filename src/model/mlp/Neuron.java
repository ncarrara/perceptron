/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.mlp;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class Neuron {

    /**
     * @return the neuron_in
     */
    public Neuron[] getNeuron_in() {
        return neuron_in;
    }

    /**
     * @return the poids_in
     */
    public double[] getPoids_in() {
        return poids_in;
    }

    /**
     * @return the last_poids_in
     */
    public double[] getNext_poids_in() {
        return next_poids_in;
    }

    public enum ActivationFunction {

        TANH, ID
    }

//    private Neuron previous_version;
    private ActivationFunction function_activation;
    private Neuron[] neuron_in;
    private double[] poids_in;
    private double[] next_poids_in;

    public Neuron(ActivationFunction activationFunction, Neuron... neurons) {
        this.function_activation = activationFunction;
        neuron_in = neurons;
        poids_in = new double[neurons.length];
        next_poids_in = new double[neurons.length];
        for (int i = 0; i < neurons.length; i++) {
            poids_in[i] = Math.random() * 2. - 1.;
        }
    }

    public void prepareToEvolve(int poid, double delta) {
        next_poids_in[poid] = poids_in[poid] + delta;
        //poids_in[poid] = poids_in[poid] + delta;
    }

    public void evolve() {
//        System.out.println(""+this+" ... elvolving");
        for (int i = 0; i < poids_in.length; i++) {
            poids_in[i] = next_poids_in[i];
        }

        for (int i = 0; i < neuron_in.length; i++) {
            neuron_in[i].evolve();
        }
    }

    public double compute() {
        return computeActivationFunction(computeLinearCombinaison());
    }

    protected double computeLinearCombinaison() {
        double somme = 0;
        for (int i = 0; i < neuron_in.length; i++) {
            somme += neuron_in[i].compute() * poids_in[i];
        }
        return somme;
    }

    protected double computeActivationFunction(double x) {
        switch (function_activation) {
            case TANH:
                return Math.tanh(x);
            case ID:
                return x;
            default:
                return Double.NaN;
        }
    }
//    protected abstract void constructPreviousVersion
}
