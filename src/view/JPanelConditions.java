/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import model.Model;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class JPanelConditions extends JPanel implements Observer {

    private final Model model;

    private JButton go = new JButton("go under these conditions");
    
    private JLabel epsilon ;
    private JLabel err ;
    
    public JPanelConditions(Model m) {
        super(new GridLayout(4, 1));

        this.model = m;
        model.addObserver(this);
        TitledBorder title;
        title = BorderFactory.createTitledBorder("Conditions d'arret");
        setBorder(title);

        JPanel pepoch = new JPanel(new GridLayout(1,2));
        pepoch.add(new JLabel("epoch to stop"));
        SpinnerModel spinnermodel = new SpinnerNumberModel(model.getEpoch_to_stop(), 0, Integer.MAX_VALUE, 1);
        JSpinner spinner = new JSpinner(spinnermodel);
        ChangeListener listener = new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSpinner js = (JSpinner)e.getSource();
                model.setEpoch_to_stop((int)js.getValue());
            }
        };

        spinner.addChangeListener(listener);
        pepoch.add(spinner);
        add(pepoch);
        
        JPanel pepsilon = new JPanel(new GridLayout(1,3));
        pepsilon.add(new JLabel("epsilon to stop"));
        epsilon = new JLabel(""+model.getError_epsilon_to_stop());
        JSlider echelle = new JSlider(JSlider.HORIZONTAL, 0, 100000, (int) (model.getError_epsilon_to_stop()*100000));
        pepsilon.add(echelle);
        echelle.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    model.setError_epsilon_to_stop(source.getValue()/100000.);
                }
            }
        });
        pepsilon.add(epsilon);
        add(pepsilon);
        
        
        JPanel perr = new JPanel(new GridLayout(1,3));
        perr.add(new JLabel("error to stop"));
        err = new JLabel(""+model.getError_to_stop());
        JSlider echeller = new JSlider(JSlider.HORIZONTAL, 0, 10000, (int) (model.getError_to_stop()*10));
        perr.add(echeller);
        echeller.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    model.setError_to_stop(source.getValue()/10.);
                }
            }
        });
        perr.add(err);
        add(perr);
        
        
        
        
        
        
        go.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                model.learnWithCondition();
            }
        });
        add(go);

    }

    @Override
    public void update(Observable o, Object arg) {
        epsilon.setText(""+model.getError_epsilon_to_stop());
        err.setText(""+model.getError_to_stop());
    }

}
