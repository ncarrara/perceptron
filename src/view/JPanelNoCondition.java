/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import model.Model;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class JPanelNoCondition extends JPanel implements Observer{
    
    private final JButton n = new JButton("run Epoch");
    private final JButton g = new JButton("go");
    private final JButton s = new JButton("stop");
    private final JButton r = new JButton("reset");
    private final JButton g100 = new JButton("go100");
    private final JButton g1000 = new JButton("go1000");
    private final JButton g10000 = new JButton("go10000");
    private final JButton rg100 = new JButton("reset & go100");
    private final JButton rg1000 = new JButton("reset & go1000");
    private final JButton rg10000 = new JButton("reset & go10000");
    private final Timer timer;
    private final Model model;
    
    public JPanelNoCondition(Model model){
        super(new GridLayout(4,3));
        model.addObserver(this);
        this.model = model;
        TitledBorder title;
        title = BorderFactory.createTitledBorder("Without conditions");
        setBorder(title);
        timer = new Timer(5, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                next();
            }
        });
        n.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                next();
            }
        });

        r.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });

        g.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                go();
            }
        });
        g100.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                go100();
            }
        });
        g10000.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                go10000();
            }
        });
        g1000.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                go1000();
            }
        });
        rg100.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
                go100();
            }
        });
        rg1000.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
                go1000();
            }
        });
        rg10000.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
                go10000();
            }
        });

        s.setEnabled(false);
        s.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        });

        add(r);
        add(n);
        add(g);
        add(g100);
        add(g1000);
       add(g10000);
        add(rg100);
        add(rg1000);
        add(rg10000);
        add(s);
    }

    public void stop() {
        n.setEnabled(true);
        g.setEnabled(true);
        g100.setEnabled(model.getP().getNbEpoch() < 100);
        g1000.setEnabled(model.getP().getNbEpoch() < 1000);
        g10000.setEnabled(model.getP().getNbEpoch() < 10000);
        timer.stop();
    }

    public void next() {
        model.runAtomique();
    }

    public void reset() {
        n.setEnabled(true);
        s.setEnabled(false);
        g.setEnabled(true);
        g100.setEnabled(true);
        g1000.setEnabled(true);
        g10000.setEnabled(true);
        timer.stop();
        model.reset();
    }

    public void resetExceptModel() {
        n.setEnabled(true);
        s.setEnabled(false);
        g.setEnabled(true);
        g100.setEnabled(true);
        g1000.setEnabled(true);
        g10000.setEnabled(true);
        timer.stop();
//        model.reset();
    }

    public void go() {
        g.setEnabled(false);
        g100.setEnabled(false);
        g1000.setEnabled(false);
        g10000.setEnabled(false);
        n.setEnabled(false);
        s.setEnabled(true);
        timer.start();
    }

    public void go100() {
        model.learnWithoutCondition(99);
        g100.setEnabled(false);
    }
   
    public void go1000() {
        model.learnWithoutCondition(999);
        g1000.setEnabled(false);
        g100.setEnabled(false);
    }

     public void go10000() {
        model.learnWithoutCondition(9999);
        g10000.setEnabled(false);
        g1000.setEnabled(false);
        g100.setEnabled(false);
    }
    
//    public void maxiteration() {
//        g.setEnabled(false);
//        n.setEnabled(false);
//        s.setEnabled(false);
//    }
    @Override
    public void update(Observable o, Object arg) {
        if (model.isReseted()) {
            resetExceptModel();
        }
        if (model.getP().getNbEpoch() >= 100) {
            g100.setEnabled(false);
        }
        if (model.getP().getNbEpoch() >= 1000) {
            g1000.setEnabled(false);
        }
        if (model.getP().getNbEpoch() >= 10000) {
            g10000.setEnabled(false);
        }
    }
}
