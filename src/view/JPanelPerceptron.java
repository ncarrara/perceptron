/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import model.ExampleGenerator.Example;
import model.Model;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class JPanelPerceptron extends JPanel implements Observer {

    private Model model;

    protected String titre;
    protected String legende;
    protected ChartPanel _chartPanel;
    private JFreeChart jfreechart;
    private ChartPanel chartpanel;
    private JCheckBox baseLearn;
    private JCheckBox baseTest;
    private JCheckBox predictLearn;
    private JCheckBox predictTest;

    public JPanelPerceptron(Model model,
            String titre,
            String legende) {

        super(new BorderLayout());
        model.addObserver(this);
        this.titre = titre;
        this.legende = legende;
        this.model = model;

        ItemListener listener = new ItemListener() {
            public void itemStateChanged(ItemEvent event) {
                renderLearningGraph();
            }
        };

        baseLearn = new JCheckBox("Base_learn");
        baseLearn.setSelected(true);
        baseLearn.addItemListener(listener);
        baseTest = new JCheckBox("Base_test");
        baseTest.setSelected(true);
        baseTest.addItemListener(listener);
        predictLearn = new JCheckBox("Predict_learn");
        predictLearn.setSelected(true);
        predictLearn.addItemListener(listener);
        predictTest = new JCheckBox("Predict_test");
        predictTest.setSelected(true);
        predictTest.addItemListener(listener);
        JPanel north = new JPanel(new GridLayout(1, 4));
        north.add(baseLearn);
        north.add(baseTest);
        north.add(predictLearn);
        north.add(predictTest);

        renderLearningGraph();
        this.add(chartpanel, BorderLayout.CENTER);
        add(north, BorderLayout.NORTH);
    }

    /**
     * Met a jour le graph des resultats
     */
    protected final void renderLearningGraph() {
        if (chartpanel != null) {
            remove(chartpanel);
        }
        revalidate();
        XYSeries seriesLearn = getLearningData();
        XYSeries seriesPredictLearn = getPredictLearningData();
        XYSeries seriesPredictTest = getPredictTestData();
        XYSeries seriesTest = getTestData();
        XYSeriesCollection dataset = new XYSeriesCollection();
        if (baseLearn.isSelected()) {
            dataset.addSeries(seriesLearn);
        }
        if (baseTest.isSelected()) {
            dataset.addSeries(seriesTest);
        }
        if (predictLearn.isSelected()) {
            dataset.addSeries(seriesPredictLearn);
        }
        if (predictTest.isSelected()) {
            dataset.addSeries(seriesPredictTest);
        }

        jfreechart = ChartFactory.createScatterPlot(titre, "x", "y", dataset, PlotOrientation.VERTICAL, false, true, true);
        jfreechart.setBackgroundPaint(Color.white);
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);

        xyplot.setAxisOffset(new RectangleInsets(4D, 4D, 4D, 4D));
        xyplot.setRangeTickBandPaint(new Color(200, 200, 100, 100));
        XYDotRenderer xydotrenderer = new XYDotRenderer();
        // xydotrenderer.setSeriesPaint(, null);
        if (dataset.getSeriesIndex("learning") >= 0) {
            xydotrenderer.setSeriesPaint(dataset.getSeriesIndex("learning"), Color.BLUE);
        }
        if (dataset.getSeriesIndex("prediction learn") >= 0) {
            xydotrenderer.setSeriesPaint(dataset.getSeriesIndex("prediction learn"), Color.GREEN);
        }
        if (dataset.getSeriesIndex("prediction test") >= 0) {
            xydotrenderer.setSeriesPaint(dataset.getSeriesIndex("prediction test"), Color.YELLOW);
        }
        if (dataset.getSeriesIndex("test") >= 0) {
            xydotrenderer.setSeriesPaint(dataset.getSeriesIndex("test"), Color.RED);
        }
        xydotrenderer.setDotWidth(4);
        xydotrenderer.setDotHeight(4);
        xyplot.setRenderer(xydotrenderer);
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
//        NumberAxis numberaxis = (NumberAxis)xyplot.getDomainAxis();
//        numberaxis.setAutoRangeIncludesZero(false);
//        xyplot.getRangeAxis().setInverted(true);
        chartpanel = new ChartPanel(jfreechart);

        add(chartpanel);
        this.repaint();

    }

    /**
     * Cree les donnee de la courbe
     *
     * @return le tableau de donnees de la courbe
     */
    protected XYSeries getLearningData() {
        XYSeries series = new XYSeries("learning", true);
        for (Example<Double, Double> e : model.getBase_learning()) {
            series.add(e.getX(), e.getY());
        }

        return series;

    }

    protected XYSeries getPredictLearningData() {
        XYSeries series = new XYSeries("prediction learn", true);
        for (Example<Double, Double> e : model.getBase_learning()) {
            series.add((double) e.getX(), (double) model.getP().predict(e.getX()));
        }
//        System.out.println("series : "+Arrays.deepToString(series.toArray()));
        return series;

    }

    protected XYSeries getTestData() {
        XYSeries series = new XYSeries("test", true);
        for (Example<Double, Double> e : model.getBase_test()) {
            series.add(e.getX(), e.getY());
        }
//        System.out.println("series : "+Arrays.deepToString(series.toArray()));
        return series;

    }

    protected XYSeries getPredictTestData() {
        XYSeries series = new XYSeries("prediction test", true);
        for (Example<Double, Double> e : model.getBase_test()) {
            series.add((double) e.getX(), (double) model.getP().predict(e.getX()));
        }
//        System.out.println("series : "+Arrays.deepToString(series.toArray()));
        return series;

    }

    @Override
    public void update(Observable o, Object arg) {
//        System.out.println("ok");
        renderLearningGraph();
        chartpanel.repaint();
    }

}
