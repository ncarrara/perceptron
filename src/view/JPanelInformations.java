/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import model.Function;
import model.Model;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class JPanelInformations extends JPanel implements Observer {

    private final Model model;

    private JLabel labelErreurLearning;
    private JLabel labelErreurTest;
    private JLabel labelepoch;
    private JLabel labelErreurAll;
    private JLabel alpha;

    String[] petStrings = {
        Function.CELSUIUS_2_FAHRENHEIT,
        Function.SIGMOID,
        Function.SINUS,
        Function.X_ABS,
        Function.X_SQUARE,
        Function.RAND};

    String[] perceptrons = {
        "simple",
        "MLP"};

    public JPanelInformations(final Model model) {
        super(new GridLayout(7, 2));
        this.model = model;
        model.addObserver(this);
        TitledBorder title;
        title = BorderFactory.createTitledBorder("Statistiques");
        setBorder(title);
        labelErreurLearning = new JLabel("NaN");
        labelErreurTest = new JLabel("NaN");
        labelErreurAll = new JLabel("NaN");
        refreshErrors();
        labelepoch = new JLabel("0");
        add(new JLabel("epoch : "));
        add(labelepoch);
        add(new JLabel("err learn : "));
        add(labelErreurLearning);
        add(new JLabel("err test : "));
        add(labelErreurTest);
        add(new JLabel("err all : "));
        add(labelErreurAll);
        add(new JLabel("fun : "));
        JComboBox petList = new JComboBox(petStrings);
        petList.setSelectedIndex(4);
        petList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                String function = (String) cb.getSelectedItem();
                model.setFunction(function);
            }
        });
        add(petList);
        add(new JLabel("perceptron : "));
        JComboBox perList = new JComboBox(perceptrons);
        perList.setSelectedIndex(1);
        perList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                String perceptron = (String) cb.getSelectedItem();
                model.setPerceptron(perceptron);
            }
        });
        add(perList);

        JPanel jp = new JPanel();//new GridLayout(1,3));
        
        jp.add(new JLabel("coeff learn"));
        JSlider echelle = new JSlider(JSlider.HORIZONTAL, 0, 1000, 100);
        echelle.setValue((int) (model.getAlpha() * 1000));
        jp.add(echelle);
        echelle.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    model.setAlpha(source.getValue() / 1000.);
                }
            }
        });
        alpha = new JLabel("" + model.getAlpha());
        jp.add(alpha);
        add(jp);
        //add(new JLabel("ok"));
    }

    @Override
    public void update(Observable o, Object arg) {
        refreshErrors();
        alpha.setText(model.getAlpha() + "");
        labelepoch.setText("" + model.getP().getNbEpoch());
    }

    public void refreshErrors() {
        NumberFormat formatter = new DecimalFormat("#0.0000");
        labelErreurLearning.setText(/*model.isReseted()?"NaN":*/"" + formatter.format(model.getP().computeTotalQuadError(model.getBase_learning())));
        labelErreurTest.setText(/*model.isReseted()?"NaN":*/"" + formatter.format(model.getP().computeTotalQuadError(model.getBase_test())));
        List cptest = new ArrayList(model.getBase_test());
        List cplean = new ArrayList(model.getBase_learning());
        cplean.addAll(cptest);
        labelErreurAll.setText(/*model.isReseted()?"NaN":*/"" + formatter.format(model.getP().computeTotalQuadError(cplean)));
    }
}
