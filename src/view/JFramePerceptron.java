/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import model.Model;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class JFramePerceptron extends JFrame implements Observer {

    public static void main(String[] args) throws IOException {

        Model m = new Model();
        new JFramePerceptron(m);
    }

    
    final Model model;

    public JFramePerceptron(final Model model/*,int infvie,int supvie,int infnaissance,int supnaissance*/) throws IOException {
        super(/*"Jeu de la vie vie : "+"["+infvie+","+supvie+"]"+" naissance : "+"["+infnaissance+","+supnaissance+"]"*/);

        JPanel south = new JPanel(/*new GridLayout(4, 3)*/);
        this.model = model;
        model.addObserver(this);
        JPanelPerceptron xyz = new JPanelPerceptron(model, "title", "legende");
        add(xyz,BorderLayout.CENTER);
        

        
        
        
        
        JPanelConditions jpc = new JPanelConditions(model);
        JPanelNoCondition jpnc = new JPanelNoCondition(model);
        
        JPanelInformations jpi = new JPanelInformations(model);
//        add(jPanelInformations, BorderLayout.EAST);
        
        JPanel east = new JPanel(new GridLayout(3,1));
        east.add(jpi);
        east.add(jpc);
        east.add(jpnc);
        add(east,BorderLayout.EAST);
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
    }

    
}
