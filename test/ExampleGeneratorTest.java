/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.List;
import model.ExampleGenerator;
import model.Function;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Pioupiou
 */
public class ExampleGeneratorTest {

    public ExampleGeneratorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void hello() {
        List l = ExampleGenerator.generateRandomExamples(
                5, Function.CELSUIUS_2_FAHRENHEIT, -Math.PI, Math.PI);
        System.out.println("examples : " + l);
        l = ExampleGenerator.normaliseExamplesAbcisse(l);
        System.out.println("normalized : " + l);
    }
}
